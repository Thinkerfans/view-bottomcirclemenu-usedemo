package com.example.testwidget;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

@SuppressLint("NewApi")
public class MeixinVideo extends Activity implements OnClickListener {



	private ImageView listenIv, talkIv, ligthIv, playIv, recordIv, mediaIv,
			snapshotIv, homeIv, addIv, minusIv, lastIv, nextIv, musicIv;

	private RelativeLayout musicRl, voiceRl, homeRl;

	private boolean misHomeRlVisiable = true;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
				WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.meixin_video);

		initView();

	}

	private void initView() {

		homeRl = (RelativeLayout) findViewById(R.id.rl_second_layout);

		homeIv = (ImageView) findViewById(R.id.iv_home);
		listenIv = (ImageView) findViewById(R.id.iv_listen);
		talkIv = (ImageView) findViewById(R.id.iv_talk);
		ligthIv = (ImageView) findViewById(R.id.iv_light);
		musicIv = (ImageView) findViewById(R.id.iv_music);

		homeIv.setOnClickListener(this);
		listenIv.setOnClickListener(this);
		talkIv.setOnClickListener(this);
		ligthIv.setOnClickListener(this);
		musicIv.setOnClickListener(this);

	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			this.finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.iv_home:
			showHomeLayout(misHomeRlVisiable);
			break;
		case R.id.iv_listen:
			break;
		case R.id.iv_talk:
			break;
		case R.id.iv_light:
			break;
		case R.id.iv_music:
			break;
		default:
			break;
		}
	}

	private void showHomeLayout(boolean visiable) {
		if (visiable) {
			misHomeRlVisiable = false;
			if (isPort) {
				MenuAnimation.startAnimationOUT(homeRl, 500);
			} 
			else {
				MenuAnimation.startAnimationOUT2(homeRl, 500);
			}

		} else {
			misHomeRlVisiable = true;
			if (isPort) {
				MenuAnimation.startAnimationIN(homeRl, 500);
			} else {
				MenuAnimation.startAnimationIN2(homeRl, 500);
			}

		}
	}

	@Override
	protected void onStop() {
		super.onStop();

	}
	
	boolean isPort = true;

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
		setContentView(R.layout.meixin_video);	
		misHomeRlVisiable = true;
		initView();
		if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			 isPort = true;		 
		} else {
			 isPort = false;
		}
	}

}
