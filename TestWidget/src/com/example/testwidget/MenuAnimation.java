package com.example.testwidget;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.RotateAnimation;

public class MenuAnimation {

	public static void startAnimationIN(ViewGroup viewGroup, int duration){
		Animation animation = new RotateAnimation(-180, 0, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 1.0f);
		animation.setDuration(duration);
		viewGroup.setVisibility(View.VISIBLE);
		viewGroup.startAnimation(animation);		
	}

	public static void startAnimationOUT(final ViewGroup viewGroup, int duration){
		Animation animation;
		animation = new RotateAnimation(0, -180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 1.0f);
		animation.setDuration(duration);
		animation.setAnimationListener(new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				viewGroup.setVisibility(View.GONE);
			}
		});
		
		viewGroup.startAnimation(animation);
	}
	
	public static void startAnimationIN2(ViewGroup viewGroup, int duration){
		Animation animation = new RotateAnimation(-90, 0, Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 1.0f);
		animation.setDuration(duration);	
		viewGroup.setVisibility(View.VISIBLE);
		viewGroup.startAnimation(animation);		
	}
	


	
	public static void startAnimationOUT2(final ViewGroup viewGroup, int duration ){
		Animation animation = new RotateAnimation(0, -90, Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 1.0f);
		animation.setDuration(duration);
		animation.setAnimationListener(new AnimationListener() {			
			@Override
			public void onAnimationStart(Animation animation) {				
			}		
			@Override
			public void onAnimationRepeat(Animation animation) {			
			}			
			@Override
			public void onAnimationEnd(Animation animation) {
				viewGroup.setVisibility(View.GONE);				
			}
		});		
		viewGroup.startAnimation(animation);
	}
	
}
